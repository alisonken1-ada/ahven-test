-------------------------------------------------------------------------------
--  test_enigma.adb                                                          --
--  Copyright (c) 2020 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Test routines for package Enigma subroutines                             --
-------------------------------------------------------------------------------

with Ahven.Framework;
with Ada.Text_IO;
with Ada.Characters.Handling;

with E_Strings_P;
with Enigma;

package body Test_Enigma is
    use Ahven;
    use Ada.Characters.Handling;
    use E_Strings_P;
    use Enigma;

    procedure Initialize (T : in out Testcase) is
    begin
        T.Set_Name (Name => "Test Enigma subroutines");
        T.Add_Test_Routine
            (Routine => Test_Get_E_Char'Access,
             Name    => "Check Get_E_Char conversion");
        T.Add_Test_Routine
            (Routine => Test_Get_E_String'Access,
             Name    => "Check Get_E_String (Normal) conversion");
        T.Add_Test_Routine
            (Routine => Test_Get_E_String_Wide'Access,
             Name    => "Check Get_E_String (Wide) conversion");
        T.Add_Test_Routine
            (Routine => Test_Get_Wiring_Option'Access,
             Name    => "Check Get_Wiring_Option (Normal)");
        T.Add_Test_Routine
            (Routine => Test_Get_Wiring_Option_Wide'Access,
             Name    => "Check Test_Get_Wiring_Option (Wide)");
    end Initialize;

    -------------------------------------------------------------------------------
    --                               Begin tests routines                        --
    -------------------------------------------------------------------------------

    procedure Test_Get_E_Char is
        Check_Char : E_Char := To_Wide_Character ('A');
        Test_Char  : E_Char := Get_E_Char ('A');
    begin
        Assert (Condition => Test_Char = Check_Char,
                Message   => "Should have returned E_Char");
    end Test_Get_E_Char;

    procedure Test_Get_E_String is
        Check_String : E_String := E_Strings_P.To_Bounded_Wide_String ("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        Test_String  : E_String := Get_E_String ("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    begin
        Assert (Condition => Test_String = Check_String,
                Message   => "Should have returned E_String");
    end Test_Get_E_String;

    procedure Test_Get_E_String_Wide is
        Check_String : E_String := E_Strings_P.To_Bounded_Wide_String ("ABCDEFGHIJKLMNOPQRSTUVXYZÅÄÖ");
        Test_String  : E_String := Get_E_String ("ABCDEFGHIJKLMNOPQRSTUVXYZÅÄÖ");
    begin
        Assert (Condition => Test_String = Check_String,
                Message   => "Should have returned E_String (Wide)");
    end Test_Get_E_String_Wide;

    procedure Test_Get_Wiring_Option is
        Check_Map : Wiring_Option_Record := (Map   => E_Strings_P.To_Bounded_Wide_String ("ABCDEF"),
                                             Notch => E_Strings_P.To_Bounded_Wide_String ("UVWXYZ")
                                            );
        Test_Map  : Wiring_Option_Record := Get_Wiring_Option (Map   => "ABCDEF",
                                                               Notch => "UVWXYZ");
    begin
        Assert (Condition => Test_Map = Check_Map,
                Message   => "Should have returned Wiring_Option_Record (Normal)");
    end Test_Get_Wiring_Option;

    procedure Test_Get_Wiring_Option_Wide is
        Check_Map : Wiring_Option_Record := (Map   => E_Strings_P.To_Bounded_Wide_String ("ABCDEFÅÄÖ"),
                                             Notch => E_Strings_P.To_Bounded_Wide_String ("ÖUVWXYZ")
                                            );
        Test_Map  : Wiring_Option_Record := Get_Wiring_Option (Map   => "ABCDEFÅÄÖ",
                                                               Notch => "ÖUVWXYZ");
    begin
        Assert (Condition => Test_Map = Check_Map,
                Message   => "Should have returned Wiring_Option_Record (Wide)");
    end Test_Get_Wiring_Option_Wide;

end Test_Enigma;
