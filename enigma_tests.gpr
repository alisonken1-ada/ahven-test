
--
--  Copyright (c) 2020 Ken Roberts
--
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your                                                                   --  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--

with "ahven";
with "enigma_common";

standard project Enigma_Tests is

    type Build_Type is ("tests", "coverage", "style");  -- "coverage" not available yet
    Build  : Build_Type := external ("BUILD");

    --  Style checks used
    --      -gnaty
    --          y4      :   4-space indentation
    --          yA      :   When using array attributes 'First, 'Last, 'Range, or 'Length,
    --                      the index number must be omitted for one-dimensional arrays and
    --                      required for multi-dimensional arrays.
    --          yb      :   Blanks not allowed at end of statements.
    --          yB      :   AND THEN/OR ELSE required except for boolean checks.
    --          yc      :   Check comment formatting (see style checking guide).
    --          yd      :   No DOS CR/LF combination. ASCII.LF only for line termination.
    --          ye      :   end/exit labels required.
    --          yf      :   No form feed/vertical tabs allowed.
    --          yh      :   No horizontal tabs allowed.
    --          yi      :   if/then layout
    --          yI      :   Mode "in" keyword is not allowed on its own. "in out" is OK.
    --          yk      :   Keywords must be lowercase.
    --          yl      :   Layout check (ex: else must line up with corresponding if, etc.).
    --          yM120   :   Line length 120 characters.
    --          yn      :   Standard entities (Integer, ASCII.NUL, etc.) match Ada RM.
    --          yo      :   Subprogram bodies must be alphabetical.
    --          yS      :   No statements after THEN/ELSE.
    --          yt      :   Token spacing required (ex. " => ").
    --          yu      :   Unnecessary blank lines not allowed.
    --          yx      :   No extra parentheses allowed in if/while/exit statements.
    --
    Compiler_Switches := Enigma_Common.Compiler_Switches;

    Style_Switches    := ("-gnaty4AbBcdefhiIklM120noStux");    -- Style checks

    Linker_Switches   := ();

    for Source_Dirs use Enigma_Common.Source_Dirs & ("tests/**");

    for Object_Dir use Enigma_Common.Obj_Dir & "/" & Build;

    case Build is
        when "style"    =>
            Compiler_Switches := Compiler_Switches & Style_Switches;
        when "tests"    =>
            Compiler_Switches := Compiler_Switches & Style_Switches;
            for Main use ("test_runner.adb");
        when "coverage" =>
                for Main use ("test_runner.adb");
                Compiler_Switches := Compiler_Switches
                & ("-ftest-coverage", "-fprofile-arcs");
            Linker_Switches   := Linker_Switches
                & ("-fprofile-generate");
    end case;

    package Compiler is
        for Switches ("Ada") use Compiler_Switches & Enigma_Common.Adaflags;
    end Compiler;

    package Linker is
        for Switches ("Ada") use Linker_Switches & Enigma_Common.Ldflags;
    end Linker;

    package Binder renames Enigma_Common.Binder;

end Enigma_Tests;
