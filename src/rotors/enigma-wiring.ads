-------------------------------------------------------------------------------
--  enigma-wiring.adb                                                        --
--  Copyright (c) 2020 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Map of known Enigma machine wiring maps                                  --
-------------------------------------------------------------------------------

with Enigma;

package Enigma.Wiring is
    Standard_Map : constant Wiring_Option_Record :=
        Enigma.Get_Wiring_Option (Map   => "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                                  Notch => "");

    Swedish_Map  : constant Wiring_Option_Record :=
        Enigma.Get_Wiring_Option (Map   => "ABCDEFGHIJKLMNOPQRSTUVXYZÅÄÖ",
                                  Notch => "");

    Wiring_Options : constant Enigma.Wiring_Option_Array :=
        (
            Enigma.Get_Wiring_Option (Map   => "EKMFLGDQVZNTOWYHXUSPAIBRCJ",    --  Option 01   Step 'Q'
                                      Notch => "Q"),
            Enigma.Get_Wiring_Option (Map   => "AJDKSIRUXBLHWTMCQGZNPYFVOE",    --  Option 02   Step 'E'
                                      Notch => "E"),
            Enigma.Get_Wiring_Option (Map   => "BDFHJLCPRTXVZNYEIWGAKMUSQO",    --  Option 03   Step 'V'
                                      Notch => "V"),
            Enigma.Get_Wiring_Option (Map   => "ESOVPZJAYQUIRHXLNFTGKDCMWB",    --  Option 04   Step 'J'
                                      Notch => "J"),
            Enigma.Get_Wiring_Option (Map   => "VZBRGITYUPSDNHLXAWMJQOFECK",    --  Option 05   Step 'Z'
                                      Notch => "Z"),
            Enigma.Get_Wiring_Option (Map   => "EJMZALYXVBWFCRQUONTSPIKHGD",    --  Option 06
                                      Notch => ""),
            Enigma.Get_Wiring_Option (Map   => "YRUHQSLDPXNGOKMIEBFZCWVJAT",    --  Option 07
                                      Notch => ""),
            Enigma.Get_Wiring_Option (Map   => "FVPJIAOYEDRZXWGCTKUQSBNMHL",    --  Option 08
                                      Notch => ""),
            Enigma.Get_Wiring_Option (Map   => "JPGVOUMFYQBENHZRDKASXLICTW",    --  Option 09   Step 'ZM'
                                      Notch => "ZM"),
            Enigma.Get_Wiring_Option (Map   => "NZJHGRCXMYSWBOUFAIVLPEKQDT",    --  Option 10   Step 'ZM'
                                      Notch => "ZM"),
            Enigma.Get_Wiring_Option (Map   => "FKQHTLXOCBJSPDZRAMEWNIUYGV",    --  Option 11   Step 'ZM'
                                      Notch => "ZM"),
            Enigma.Get_Wiring_Option (Map   => "LEYJVCNIXWPBQMDRTAKZGFUHOS",    --  Option 12
                                      Notch => ""),
            Enigma.Get_Wiring_Option (Map   => "FSOKANUERHMBTIYCWLQPZXVGJD",    --  Option 13
                                      Notch => ""),
            Enigma.Get_Wiring_Option (Map   => "ENKQAUYWJICOPBLMDXZVFTHRGS",    --  Option 14
                                      Notch => ""),
            Enigma.Get_Wiring_Option (Map   => "RDOBJNTKVEHMLFCWZAXGYIPSUQ",    --  Option 15
                                      Notch => ""),
            Enigma.Get_Wiring_Option (Map   => "VEOSIRZUJDQCKGWYPNXAFLTHMB",    --  Option 16   Step 'Q'
                                      Notch => "Q"),
            Enigma.Get_Wiring_Option (Map   => "UEMOATQLSHPKCYFWJZBGVXINDR",    --  Option 17   Step 'E'
                                      Notch => "E"),
            Enigma.Get_Wiring_Option (Map   => "TZHXMBSIPNURJFDKEQVCWGLAOY",    --  Option 18   Step 'V'
                                      Notch => "V"),
            Enigma.Get_Wiring_Option (Map   => "WTOKASUYVRBXJHQCPZEFMDINLG",    --  Option 19   Step 'Q'
                                      Notch => "Q"),
            Enigma.Get_Wiring_Option (Map   => "GJLPUBSWEMCTQVHXAOFZDRKYNI",    --  Option 20   Step 'E'
                                      Notch => "E"),
            Enigma.Get_Wiring_Option (Map   => "JWFMHNBPUSDYTIXVZGRQLAOEKC",    --  Option 21   Step 'V'
                                      Notch => "V"),
            Enigma.Get_Wiring_Option (Map   => "HEJXQOTZBVFDASCILWPGYNMURK",    --  Option 22   Step 'Z'
                                      Notch => "Z"),
            Enigma.Get_Wiring_Option (Map   => "MOWJYPUXNDSRAIBFVLKZGQCHET",    --  Option 23
                                      Notch => ""),
            Enigma.Get_Wiring_Option (Map   => "CIAGSNDRBYTPZFULVHEKOQXWJM",    --  Option 24
                                      Notch => ""),
            Enigma.Get_Wiring_Option (Map   => "LPGSZMHAEOQKVXRFYBUTNICJDW",    --  Option 25   Step 'SUVWZABCEFGIKLOPQ'
                                      Notch => "SUVWZABCEFGIKLOPQ"),
            Enigma.Get_Wiring_Option (Map   => "SLVGBTFXJQOHEWIRZYAMKPCNDU",    --  Option 26   Step 'STVYZACDFGHKMNQ'
                                      Notch => "STVYZACDFGHKMNQ"),
            Enigma.Get_Wiring_Option (Map   => "CJGDPSHKTURAWZXFMYNQOBVLIE",    --  Option 27   Step 'UWXAEFHKMNR'
                                      Notch => "UWXAEFHKMNR"),
            Enigma.Get_Wiring_Option (Map   => "IMETCGFRAYSQBZXWLHKDVUPOJN",    --  Option 28
                                      Notch => ""),
            Enigma.Get_Wiring_Option (Map   => "QWERTZUIOASDFGHJKPYXCVBNML",    --  Option 29
                                      Notch => ""),
            Enigma.Get_Wiring_Option (Map   => "DMTWSILRUYQNKFEJCAZBPGXOHV",    --  Option 30   Step 'SUVWZABCEFGIKLOPQ'
                                      Notch => "SUVWZABCEFGIKLOPQ"),
            Enigma.Get_Wiring_Option (Map   => "HQZGPJTMOBLNCIFDYAWVEUSRKX",    --  Option 31   Step 'STVYZACDFGHKMNQ'
                                      Notch => "STVYZACDFGHKMNQ"),
            Enigma.Get_Wiring_Option (Map   => "UQNTLSZFMREHDPXKIBVYGJCWOA",    --  Option 32   Step 'UWXAEFHKMNR'
                                      Notch => "UWXAEFHKMNR"),
            Enigma.Get_Wiring_Option (Map   => "RULQMZJSYGOCETKWDAHNBXPVIF",    --  Option 33
                                      Notch => ""),
            Enigma.Get_Wiring_Option (Map   => "RCSPBLKQAUMHWYTIFZVGOJNEXD",    --  Option 34   Step 'SUVWZABCEFGIKLOPQ'
                                      Notch => "SUVWZABCEFGIKLOPQ"),
            Enigma.Get_Wiring_Option (Map   => "WCMIBVPJXAROSGNDLZKEYHUFQT",    --  Option 35   Step 'STVYZACDFGHKMNQ'
                                      Notch => "STVYZACDFGHKMNQ"),
            Enigma.Get_Wiring_Option (Map   => "FVDHZELSQMAXOKYIWPGCBUJTNR",    --  Option 36   Step 'UWXAEFHKMNR'
                                      Notch => "UWXAEFHKMNR"),
            Enigma.Get_Wiring_Option (Map   => "WLRHBQUNDKJCZSEXOTMAGYFPVI",    --  Option 37   Step 'SUVWZABCEFGIKLOPQ'
                                      Notch => "SUVWZABCEFGIKLOPQ"),
            Enigma.Get_Wiring_Option (Map   => "TFJQAZWMHLCUIXRDYGOEVBNSKP",    --  Option 38   Step 'STVYZACDFGHKMNQ'
                                      Notch => "STVYZACDFGHKMNQ"),
            Enigma.Get_Wiring_Option (Map   => "QTPIXWVDFRMUSLJOHCANEZKYBG",    --  Option 39   Step 'SWZFHMQ'
                                      Notch => "SWZFHMQ"),
            Enigma.Get_Wiring_Option (Map   => "LPGSZMHAEOQKVXRFYBUTNICJDW",    --  Option 40   Step 'Y'
                                      Notch => "Y"),
            Enigma.Get_Wiring_Option (Map   => "SLVGBTFXJQOHEWIRZYAMKPCNDU",    --  Option 41   Step 'E'
                                      Notch => "E"),
            Enigma.Get_Wiring_Option (Map   => "CJGDPSHKTURAWZXFMYNQOBVLIE",    --  Option 42   Step 'N'
                                      Notch => "N"),
            Enigma.Get_Wiring_Option (Map   => "PEZUOHXSCVFMTBGLRINQJWAYDK",    --  Option 43   Step 'Y'
                                      Notch => "Y"),
            Enigma.Get_Wiring_Option (Map   => "ZOUESYDKFWPCIQXHMVBLGNJRAT",    --  Option 44   Step 'E'
                                      Notch => "E"),
            Enigma.Get_Wiring_Option (Map   => "EHRVXGAOBQUSIMZFLYNWKTPDJC",    --  Option 45   Step 'N'
                                      Notch => "N"),
            Enigma.Get_Wiring_Option (Map   => "VEZIOJCXKYDUNTWAPLQGBHSFMR",    --  Option 46   Step 'SUYAEHLNQ'
                                      Notch => "SUYAEHLNQ"),
            Enigma.Get_Wiring_Option (Map   => "HGRBSJZETDLVPMQYCXAOKINFUW",    --  Option 47   Step 'SUYAEHLNQ'
                                      Notch => "SUYAEHLNQ"),
            Enigma.Get_Wiring_Option (Map   => "NWLHXGRBYOJSAZDVTPKFQMEUIC",    --  Option 48   Step 'SUYAEHLNQ'
                                      Notch => "SUYAEHLNQ"),
            Enigma.Get_Wiring_Option (Map   => "NSUOMKLIHZFGEADVXWBYCPRQTJ",    --  Option 49
                                      Notch => ""),
            Enigma.Get_Wiring_Option (Map   => "JGDQOXUSCAMIFRVTPNEWKBLZYH",    --  Option 50   Step 'N'
                                      Notch => "N"),
            Enigma.Get_Wiring_Option (Map   => "NTZPSFBOKMWRCJDIVLAEYUXHGQ",    --  Option 51   Step 'E'
                                      Notch => "E"),
            Enigma.Get_Wiring_Option (Map   => "JVIUBHTCDYAKEQZPOSGXNRMWFL",    --  Option 52   Step 'Y'
                                      Notch => "Y"),
            Enigma.Get_Wiring_Option (Map   => "QYHOGNECVPUZTFDJAXWMKISRBL",    --  Option 53
                                      Notch => ""),
            Enigma.Get_Wiring_Option (Map   => "PSBGÖXQJDHOÄUCFRTEZVÅINLYMKA",  --  Option 54  Step 'Ä'
                                      Notch => "Ä"),
            Enigma.Get_Wiring_Option (Map   => "CHNSYÖADMOTRZXBÄIGÅEKQUPFLVJ",  --  Option 55  Step 'Ä'
                                      Notch => "Ä"),
            Enigma.Get_Wiring_Option (Map   => "ÅVQIAÄXRJBÖZSPCFYUNTHDOMEKGL",  --  Option 56  Step 'Ä'
                                      Notch => "Ä"),
            Enigma.Get_Wiring_Option (Map   => "LDGBÄNCPSKJAVFZHXUIÅRMQÖOTEY",  --  Option 57
                                      Notch => "")
        );

end Enigma.Wiring;