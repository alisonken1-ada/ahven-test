-------------------------------------------------------------------------------
--  enigma.adb                                                               --
--  Copyright (c) 2020 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Base package and subroutines for Enigma emulator                         --
-------------------------------------------------------------------------------

with Ada.Characters.Handling; use Ada.Characters.Handling;

package body Enigma is

    function Get_E_Char (Source : Character)
        return E_Char is
    begin
        return To_Wide_Character (Source);
    end Get_E_Char;

    function Get_E_String (Source : String)
        return E_String is
    begin
        return E_Strings_P.To_Bounded_Wide_String (To_Wide_String (Source));
    end Get_E_String;

    function Get_Wiring_Option (Map   : String;
                                Notch : String)
        return Wiring_Option_Record is
    begin
        return (Map   => Get_E_String (Map),
                Notch => Get_E_String (Notch));
    end Get_Wiring_Option;

end Enigma;