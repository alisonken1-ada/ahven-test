-------------------------------------------------------------------------------
--  enigma.ads                                                               --
--  Copyright (c) 2020 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Base package and subroutines for Enigma emulator                         --
-------------------------------------------------------------------------------

with E_Strings_P;

package Enigma is

    Pins_Standard    : constant Integer := 26;
    --  If you change Pins_NonStandard don't forget to update E_Strings_P.ads
    Pins_NonStandard : constant Integer := 28;

    subtype E_Char   is Wide_Character;
    subtype E_String is E_Strings_P.Bounded_Wide_String;

    --  Defines the known wiring maps
    type Wiring_Option_Record is record
        Map   : E_String;
        Notch : E_String;
    end record;
    type Wiring_Option_Array is array (01 .. 57) of Wiring_Option_Record;

    --  Converts a standard string to E_String
    function Get_E_Char (Source : Character)
        return E_Char;

    --  Converts a character to E_Char
    function Get_E_String (Source : String)
        return E_String;

    --  Helper to build Wiring.Wiring_Options (known wiring maps);
    function Get_Wiring_Option (Map   : String;
                                Notch : String)
        return Wiring_Option_Record;

end Enigma;
