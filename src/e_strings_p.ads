-------------------------------------------------------------------------------
--  e_strings_p.ads                                                          --
--  Copyright (c) 2020 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Wide bounded string package for Enigma strings                           --
-------------------------------------------------------------------------------

with Ada.Strings.Wide_Bounded;

--  Must use constant here, but keep the same as Enigma.Pins_NonStandard
--
--  Exception: Ahven test suite does not like wide characters, so add an
--             extra byte for each of the characters outside of range A .. Z
--  Example:   E_String is normally 26 characters "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
--             E_String with Swedish character    "ABCDEFGHIJKLMNOPQRSTUVWXYZÅ"
--             The 'Å' requires 2 bytes, so to add 3 Swedish characters
--                 E_String "ABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ"
--             requires 3 extra bytes.
--             Swedish Enigma removes the 'W', so only 28 characters are needed for
--             the string, but an extra 3 bytes are needed for the Swedish enigma.
package E_Strings_P is new Ada.Strings.Wide_Bounded.Generic_Bounded_Length (Max => 32);
