
Copyright (c) 2020 Ken Roberts (alisonken1 at gmail)

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
