
Program considerations when designing rotors.

Defines
    - Input         - Normally keyboard
    - Stecker       - Plugboard provides 1-1 letter exchange (A->Z means Z->A on reflect)
    - ETW           - Connects stecker to rotor 1. May also have pin swap
    - Rotor         - 1-5 rotors depending on model
                        - Some Enigma models have a mixture of fixed and stepping rotors
    - UKW           - Reflector routes signal back through rotors
                        - Some models have steping UKW
                        - Some models have field-changeable UKW (UKW-D)
    - Output        - Show encrypted letter
                        - Most models used lamps
                        - Some models used paper printout

Pin Count
    - Standard rotor 26
    - Swedish Enigma B (s/n A-133/A-134) rotor has 28 pins
    - At least one other model had 28 pin rotors

Keyboard
    - Most models have 1-1 corespondence keys -> letters
    - One model had 29 keys; 28 mapped keys with "X" input
      hardwired to "X" output

Stepping right-to-left order [(4) 3 2 1]
    - Rotor 1 steps every key press
        - Some models rotor 2 stepped every keypress instead of rotor 1
    - Rotor 2
        - Some models rotor 2 stepped every keypress instead of rotor 1
        - Gear model only steps when notch in Rotor 1 in step position
        - Pawl model steps when
            - Notch in Rotor 1 under pawl
            - Notch in Rotor 1 under pawl AND notch in rotor 2 under pawl
    - Rotor 3 steps when Notch in Rotor 2 under step position
    - Rotor 4 steps when Notch in Rotor 3 under step position

Encryption
    - Pin-to-pin correspondence with keys
        - One model mapped "X" straight to output "X", other keys mapped normally
    - Calculate based on input pin + rotor position

Stepping
    - Rotor position (4-3-2-1) determines stepping sequence
    - Calculate based on
        - (Notch on ring) rotor position + ring offset
        - (Notch on rotor) rotor position
    - Some models have fixed rotor 1
    - Some models have fixed rotor 4
    - Some models have stepping UKW

Encryption flow
    - Map chassis pin to rotor input pin (input pin + rotor position)
    - Map rotor input pin to rotor output pin
    - Map rotor output pin to chassis pin (rotor position + output pin)
    - Update ring display (rotor position + ring offset)
