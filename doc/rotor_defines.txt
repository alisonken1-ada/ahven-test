This file shows the mappings of the rotor types based on the enigma model.

See Notes.txt for how the Ringstellung (Ring) setting affects rotor setup.

Information used is available at
    http://www.cryptomuseum.com/crypto/enigma/wiring.htm


NOTES:  Rotors will rotate BEFORE encryption.

Wheel    = letter displayed in Enigma rotor window
Notch    = Physical location of rotation notch related to rotor letter
Turnover = Letter dislayed in rotor window in relation to Notch
ETW      = Hidden wheel between plugboard and first rotor
UKW      = Hidden loopback wheel (reflector) at end of rotors
           UKW-D is field rewireable

Standard Enigma signal flow:
    1   Keyboard
    2   * Plugboard
    3   * ETW
    4   Rotor(s) (rotating) (2-4)
    5   * Rotor (fixed)
    5   UKW (reflector)
    6   * Rotor (fixed) (reflected)
    6   Rotor(s) (rotating) (reflected)
    7   * ETW (reflected)
    8   * Plugboard (reflected)
    9   Lamps/Printer

* NOTE: Available on selected models only

The position of the notch is different for each of the rotors. In the table
below we can see that rotor I has a notch on letter Y. If this notch is
positioned in front of the pawl, the letter Q is visible in the little
window. Therefore, the rotor to the left of rotor I will step if rotor I
steps from Q to R.

(cryptanalists used the mnemonic "Royal Flags Wave Kings Above" to
remember the positions of the rotors after turnover).

Notice that the Kriegsmarine rotors VI, VII and VIII have two notches.
This advances the next rotor twice as fast as the other rotors.

Rotor           Notch   Window          next left rotor steps
                                        when rotor steps from -> to
I               Y       Q               Q -> R
II              M       E               E -> F
III             D       V               V -> W
IV              R       J               J -> K
V               H       Z               Z -> A
VI/VII/VIII     H + U   Z + M           Z -> A, M-> N

=============================================================================
Enigma I
￼German Army and Air Force (Wehrmacht, Luftwaffe)

The Enigma I was the main Enigma machine used by the German Army. The Army
and Navy machines were the only ones with a plug board. Below is the wiring
for each wheel, the ETW and all three known UKWs. UKW-A was used before WWII.
UKW-B was the standard reflector during the war and UKW-C was only used in
the later part of the war. The wiring of all 5 wheels is identical to the
wiring of the first 5 wheels of the Enigma M3 (Navy) and the U-Boot Enigma M4.

Wheel   ABCDEFGHIJKLMNOPQRSTUVWXYZ      Notch   Turnover        #
ETW     ABCDEFGHIJKLMNOPQRSTUVWXYZ
I       EKMFLGDQVZNTOWYHXUSPAIBRCJ      Y       Q               1
II      AJDKSIRUXBLHWTMCQGZNPYFVOE      M       E               1
III     BDFHJLCPRTXVZNYEIWGAKMUSQO      D       V               1
IV      ESOVPZJAYQUIRHXLNFTGKDCMWB      R       J               1
V       VZBRGITYUPSDNHLXAWMJQOFECK      H       Z               1
UKW-A   EJMZALYXVBWFCRQUONTSPIKHGD
UKW-B   YRUHQSLDPXNGOKMIEBFZCWVJAT
UKW-C   FVPJIAOYEDRZXWGCTKUQSBNMHL


=============================================================================
Enigma M3
German Navy (Kriegsmarine)

The Enigma M1, M2 and M3 machines were used by the German Navy (Kriegsmarine).
They are basically compatible with the Enigma I.
The wiring of the Enigma M3 is given in the table below.
Wheels I thru V are identical to those of the Enigma I.
The same is true for UKW B and C.
The three additional wheels (VI, VII and VIII) were used exclusively by the
Kriegsmarine.
The machine is also compatible with the Enigma M4 (when the 4th wheel of the
M4 is set to 'A').

Wheel   ABCDEFGHIJKLMNOPQRSTUVWXYZ      Notch   Turnover        #
ETW     ABCDEFGHIJKLMNOPQRSTUVWXYZ
I       EKMFLGDQVZNTOWYHXUSPAIBRCJ      Y       Q               1
II      AJDKSIRUXBLHWTMCQGZNPYFVOE      M       E               1
III     BDFHJLCPRTXVZNYEIWGAKMUSQO      D       V               1
IV      ESOVPZJAYQUIRHXLNFTGKDCMWB      R       J               1
V       VZBRGITYUPSDNHLXAWMJQOFECK      H       Z               1
VI      JPGVOUMFYQBENHZRDKASXLICTW      HU      ZM              2
VII     NZJHGRCXMYSWBOUFAIVLPEKQDT      HU      ZM              2
VIII    FKQHTLXOCBJSPDZRAMEWNIUYGV      HU      ZM              2
UKW-B   YRUHQSLDPXNGOKMIEBFZCWVJAT
UKW-C   FVPJIAOYEDRZXWGCTKUQSBNMHL


=============================================================================
Enigma M4
￼U-Boot Enigma

The Enigma M4 was a further development of the M3 and was used exclusively by
the U-Boot division of the German Navy (Kriegsmarine).
It was introduced unexpectedly on 2 February 1942.

Below is the wiring for each wheel, the ETW and all known UKWs.
UKW-B was the standard reflector throughout the war and UKW-C was only
temporarily used during the war.

The wiring of the first 5 wheels (I-V) is identical to the wiring of the 5
wheels of the Enigma I used by the Wehrmacht and Luftwaffe.
This allowed secure communication between the departments.

The three extra wheels (VI, VII and VIII) have two notches each, which causes
a more frequent wheel turnover, but also introduces another weakness.

Wheel   ABCDEFGHIJKLMNOPQRSTUVWXYZ      Notch   Turnover        #
ETW     ABCDEFGHIJKLMNOPQRSTUVWXYZ
I       EKMFLGDQVZNTOWYHXUSPAIBRCJ      Y       Q               1
II      AJDKSIRUXBLHWTMCQGZNPYFVOE      M       E               1
III     BDFHJLCPRTXVZNYEIWGAKMUSQO      D       V               1
IV      ESOVPZJAYQUIRHXLNFTGKDCMWB      R       J               1
V       VZBRGITYUPSDNHLXAWMJQOFECK      H       Z               1
VI      JPGVOUMFYQBENHZRDKASXLICTW      HU      ZM              2
VII     NZJHGRCXMYSWBOUFAIVLPEKQDT      HU      ZM              2
VIII    FKQHTLXOCBJSPDZRAMEWNIUYGV      HU      ZM              2
Beta    LEYJVCNIXWPBQMDRTAKZGFUHOS
Gamma   FSOKANUERHMBTIYCWLQPZXVGJD
UKW-B   ENKQAUYWJICOPBLMDXZVFTHRGS
UKW-C   RDOBJNTKVEHMLFCWZAXGYIPSUQ


=============================================================================
Enigma KD
￼Enigma K with UKW-D

The Enigma KD was a standard commercial Enigma K machine with a rewirable
reflector (UKW-D).

Below is the wiring if the first three wheels (I, II and III) of the Enigma KD
that was found in the archives of the FRA [8].
This wiring might be identical to the first three wheels of the Enigma KD
used by Mil Amt during WWII, but this is currently uncertain.

Wheel   ABCDEFGHIJKLMNOPQRSTUVWXYZ      Notch           Turnover        #
ETW     QWERTZUIOASDFGHJKPYXCVBNML
I       VEZIOJCXKYDUNTWAPLQGBHSFMR      ACGIMPTVY       SUYAEHLNQ       9
II      HGRBSJZETDLVPMQYCXAOKINFUW      ACGIMPTVY       SUYAEHLNQ       9
III     NWLHXGRBYOJSAZDVTPKFQMEUIC      ACGIMPTVY       SUYAEHLNQ       9
UKW     NSUOMKLIHZFGEADVXWBYCPRQTJ                      *

* Note that due to the nature of the (rewirable) UKW it does not have a fixed
wiring. The table above shows the wiring of the UKW when the machine was
discovered at the FRU. The actual wiring will have been changed frequently
when the machine was used in an operational context.

Mil Amt changed the order of the wheels and the Ringstellung daily, whilst
the Grundstellung (and probably also the wiring of UKW-D) was changed every
three weeks.

=============================================================================
=============================================================================

Non-Military Enigma

=============================================================================
=============================================================================

Enigma D
Commercial Enigma A26

Developed in 1926 as a successor to the Enigma C. Replaced in 1927 with
the Enigma K.

The  Enigma D contained many improvements over the Enigma C. First of all, the
top lid of the machine was made more accessible, so that it was easier to
alter the basic settings (key). The three coding wheels were now mounted on
a removable spindle, so that the order of the wheels could be changed as well.
Furthermore, the reflector (UKW) became settable, which means that it could
be set to any of 26 positions. All this increased the maximum number of
permutations.

As a result, 4 wheels protrude the top lid and hence there are 4 windows
through which the current settings can be viewed. Because of this, it is
sometimes assumed that this is a 4-wheel Enigma machine. Although strictly
speaking there are 4 wheels, the leftmost one is the UKW. It should
therefore be defined as a 3-wheel machine with a settable UKW.

Descendants

Reichswehr D (Ch. 11a)
Enigma K (A27, Ch. 11b)
Zählwerk Enigma (A28, Ch. 15)
Enigma Z (Z30, Ch. 16)

The wheels of Enigma A818 (see above) contain the standard commercial wiring
as presented in the table below [1]. This wiring was identical for all
commercial machines, including the later Enigma K (A27). Although the wiring
of the wheels was changed by some customers, they often left the wiring of the
UKW intact. As far as we know, the wiring of the ETW was never changed.

Wheel   ABCDEFGHIJKLMNOPQRSTUVWXYZ  Notch   Turnover    #
ETW     QWERTZUIOASDFGHJKPYXCVBNML
I       LPGSZMHAEOQKVXRFYBUTNICJDW  G       Y           1
II      SLVGBTFXJQOHEWIRZYAMKPCNDU  M       E           1
III     CJGDPSHKTURAWZXFMYNQOBVLIE  V       N           1
UKW     IMETCGFRAYSQBZXWLHKDVUPOJN

=============================================================================

Enigma K
A family of commercial machines A27

In 1927, developments were started to create improved versions of the
commercial Enigma D machine. One of the most important offsprings was the
Enigma K that was given the model number A27 and internal designator Ch. 11b.
The letter 'K' was probably used for the German word Kommerziell (commercial).
Apart from minor manufacturing differences, this machine is identical to the
Enigma D. The machine was supplied to a variety of (international) customers.

K Variants

Swiss Enigma K variant - 1938
Enigma T (Tirpitz) 1942
Enigma KD - 1944
Reichsbahn (Railway) Enigma - 1940

The wiring of the wheels of the standard Enigma K was identical to the wiring
of the Enigma D. This suggests that the machine was initially intended for
commercial customers. The standard commercial wiring is given in the table
below.

Wheel   ABCDEFGHIJKLMNOPQRSTUVWXYZ  Notch   Turnover    #
ETW     QWERTZUIOASDFGHJKPYXCVBNML
I       LPGSZMHAEOQKVXRFYBUTNICJDW  G       Y           1
II      SLVGBTFXJQOHEWIRZYAMKPCNDU  M       E           1
III     CJGDPSHKTURAWZXFMYNQOBVLIE  V       N           1
UKW     IMETCGFRAYSQBZXWLHKDVUPOJN

=============================================================================

Enigma B


Enigma B is a very early glow lamp-based Enigma machine, that is the successor
to the Enigma A and pre-dates the Enigma C and Enigma D. It was introduced in
late 1924 and was available in several variants, including one with 28 letters,
instead of the more common 26. It was the first in the series of
Glühlampenchiffriermaschinen (glow lamp cipher machines) that had removable
rotors (allowing their order to be changed) and a settable ring on each
wheel (Ringstellung).


Wiring of the A-133


A-133 was a special variant of the Enigma B, that was delivered to the Swedish SGS
on 6 April 1925 [13]. It has 28 letters on the keyboard and on the lamp panel.
Likewise, it has 28 contacts at either side of each wheel, rather than the more
common 26. This shows that it was a 'special'. Note that rotor (I) has letters
on the ring, whilst rotors (II) and (III) have numbers. To make the table more
uniform, we have translated the numbers into letters, using the following scheme:

01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28
A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  X  Y  Z  Å  Ä  Ö

The extra letters are Å, Ä and Ö, which are frequently used in the Swedish language.
Furthermore the letter 'W' is missing. It is not used in written Swedish language,
except for names, loan words, foreign words, etc. When needed, the 'W' was replaced
by 'V' or 'VV' (double-V).

    Wheel   ABCDEFGHIJKLMNOPQRSTUVXYZÅÄÖ    Notch   Turnover    #
    ETW     ABCDEFGHIJKLMNOPQRSTUVXYZÅÄÖ
    I       PSBGÖXQJDHOÄUCFRTEZVÅINLYMKA    G       Ä           1
    II *    CHNSYÖADMOTRZXBÄIGÅEKQUPFLVJ    G       Ä           1
    III *   ÅVQIAÄXRJBÖZSPCFYUNTHDOMEKGL    G       Ä           1
    UKW     LDGBÄNCPSKJAVFZHXUIÅRMQÖOTEY

    * This wheel has numbers (01-28) rather than letters (A-Ö).

    [13] Anders Wik, The First Classical Enigmas,
    Swedish Views on Enigma Development 1924-1930
    HistoCrypt 2018, Proceedings. 18-20 June 2018, pp. 83-88.
